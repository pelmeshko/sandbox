export interface Person {
    id?: number,
    firstName?: string,
    lastName?: string,
    hasChild?: boolean,
}
export default interface Family {
    id?: number,
    husband?: Person
    wife?: Person,
    children?: Person[],
}