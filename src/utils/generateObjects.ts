import {Person} from "../types/Family";

const generatePerson:()=>Person = ()=>({
    id: null,
    firstName: '',
    lastName: '',
    hasChild: false,
})

export {generatePerson}