import {Checkbox, Form, FormInput, Label} from "semantic-ui-react";
import React, {useCallback} from "react";
import {Person} from "../types/Family";

interface PersonComponentProps {
    person: Person,
    onPersonChange: (person: Person) => void,
}
export default function PersonComponent (props: PersonComponentProps) {
    const {person} = props;
    const handleChange =useCallback((e: any, data: any) => {
        console.log("e",e)
        console.log("data",data)
        const newPerson = {
            ...person,
            [data.name]: data.value
        }
        props.onPersonChange(newPerson)
    }, [person]);
    return <Form>
        <FormInput
            placeholder='Имя'
            name={"firstName"}
            value={person.firstName}
            onChange={handleChange}
        />
        <Label content={"test"}/>
        <Checkbox label='Есть дети' checked={person.hasChild}/>
    </Form>
}
