import React, {useCallback, useMemo, useState} from "react";
import {Button} from "semantic-ui-react";
import Family, {Person} from "../types/Family";
import PersonComponent from "./PersonComponent";
import ChildrenComponent from "./ChildrenComponent";

interface FamilyProps {
    family: Family
}

export default function FamilyForm(props: FamilyProps) {
    const [family, setFamily] = useState(props.family);

    const onSubmit = useCallback(() => {
        console.log("family", family)
    }, [family]);

    // const handleChange = useCallback((key: string, person: Person) => {
    //     console.log("changedPerson:", person)
    //     setFamily({ ...family, [key]: person });
    // }, [])

    const handleParentsChange = useCallback((key: string)=> {
        return  (person: Person) => {
            console.log("changedPerson:", person, key)

            setFamily(prevState => ({
                    ...prevState,
                    [key]: person,
                })
            );
        }
    }, [])
    console.log(family)

    const wifeHandler = useCallback((person: Person)=> { handleParentsChange('wife')(person)} ,[])
    const husbandHandler = useCallback((person: Person)=> { handleParentsChange('husband')(person)} ,[])
    const onChildrenChanged = useCallback((children: Family["children"])=>{
        setFamily(prev=>({...prev, children: children}))
    }, [])

    return (
        <>
            <h2>Редактирование семьи</h2>
            <PersonComponent onPersonChange={wifeHandler} person={family.wife}/>
            <PersonComponent onPersonChange={husbandHandler} person={family.husband}/>
            <ChildrenComponent onChildrenChanged={onChildrenChanged} children={family.children}/>
            <Button type='submit' onClick={onSubmit}>Submit</Button>
        </>
    );
}
