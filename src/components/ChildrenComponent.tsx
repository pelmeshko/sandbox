import React, {useCallback} from "react";
import Family, {Person} from "../types/Family";
import PersonComponent from "./PersonComponent";
import {Button, Segment} from "semantic-ui-react";
import {generatePerson} from "../utils/generateObjects";
export interface ChildrenComponentProps{
    children: Family["children"],
    onChildrenChanged: (children: Family["children"]) => void,
}
export default function ChildrenComponent(props: ChildrenComponentProps){
    const {onChildrenChanged, children} = props;
    const removeChild = (index: number)=>{
        //TODO: удаление

    }

    const addChild = useCallback(()=>{
        onChildrenChanged([...children, generatePerson()])
    }, [children])

    const onChildChange = useCallback((person: Person)=>{
        //TODO: изменние

    }, [])
    return <Segment>
        <h3>Дети</h3>
        <Button onClick={addChild} content={'Добавить'}/>
        {children.map((child, index)=> {
            /*
                TODO: тут явно надо не индекс. ID тоже не могу, т.к. при создании ID нет.
                ID который в Person - это то что в БД хранится. И при создании не надо его передавать
                Как по-нормальному это сделать? Если в самом объекте нет уникальных значений.
            * */
            return <Segment key={index}>
                <PersonComponent person={child} onPersonChange={onChildChange}/>
                <Button onClick={() => removeChild(index)} content={'Удалить'}/>
            </Segment>
        })}
    </Segment>
}