import React from "react";
import FamilyForm from "./components/FamilyForm";
import Family from "./types/Family";
import 'semantic-ui-css/semantic.min.css'
import {Container} from "semantic-ui-react";


const families: Family[] = [
    {
        id: 1,
        husband: {
            firstName: 'Михаил',
        },
        wife: {
            firstName: 'Евдокия',
        },
        children: [
            {firstName: 'Алексей'},
            {firstName: 'Клавдия'},
        ]
    },
    {
        id: 2,
        husband: {
            firstName: 'Игорь',
        },
        wife: {
            firstName: 'Ольга',
        },
        children: [
            {firstName: 'Святослав'},
        ]
    },
]

export default function App() {
    return <Container>
        <FamilyForm family={families[0]}/>
    </Container>
}
